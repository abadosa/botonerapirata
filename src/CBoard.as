package  
{
	import flash.display.Sprite;
	/**
	 * ...
	 * @author Albert Badosa Solé
	 */
	public class CBoard extends Sprite
	{
		public static const HORIZONTAL_MARGINS:Number = 8.0;
		public static const VERTICAL_MARGINS:Number = 140.0;
		public static const HORIZONTAL_GAP:Number = 5.0;
		public static const VERTICAL_GAP:Number = 15.0;
		public static const BUTTONS_PER_PAGE:uint = 55;
		
		private var header:TitleBar;
		private var buttons:Array;
		private var navigator:CNavigator;
		
		private var currentPage:uint = 0;
		
		protected var boardName:String;
		
		public function CBoard(boardName:String = Main.PIRATA_NAME) 
		{
			super();
			
			if (boardName != null && boardName != "") this.boardName = boardName;
		}
		
		public function init():void
		{
			header = new TitleBar();
			header.x = (Main.customStage.stageWidth - header.width) * 0.5;
			header.y = 105.0;
			addChild(header);
			
			navigator = new CNavigator();
			navigator.x = Main.customStage.stageWidth - navigator.width - HORIZONTAL_MARGINS;
			navigator.y = Main.customStage.stageHeight - navigator.height - VERTICAL_GAP;
			addChild(navigator);
			
			buttons = new Array();
			
			SetupTitleBar();
			SetupButtons();
			SetupButtonPositions();
			SetupNavigator();
		}
		
		private function SetupTitleBar():void
		{
			if(boardName == Main.PIRATA_NAME) {
				var randomTitles:Array = new Array(
					"PIRATA", "DE-DEVU", "TIM RYANS", "SASINS-CREY-TESHIONS", "HOJATANDO",
					"CORTA-LABIOS", "YO CONOZCO", "EL GUEIMA", "CUATRO SUSARIOS", "TONTERÍA",
					"PHIL FISH'S", "LUFTRAUSERS", "ALBERTO", "YO CONOSCOW", "OS MORTEIRÕS",
					"WANNABRODAS", "MAX!"
				);
				
				header.title.text = header.title_shadow.text = "\"" + randomTitles[Math.floor(Math.random() * (randomTitles.length))] + "\" BOARD";
			} else {
				header.title.text = header.title_shadow.text = "\"" + boardName.toUpperCase() + "\" BOARD";
			}
		}
		
		private function SetupButtons():void
		{
			buttons.length = 0;
			
			switch(boardName) {
				case Main.PIRATA_NAME:
					buttons.push(new CSoundButton("Modulus de multijugadores", "modulus_de_multijugadores"));
					buttons.push(new CSoundButton("Bautizando el interfaz", "bautizando_el_interfaz"));
					buttons.push(new CSoundButton("Buenas noches y bienvenidos", "buenas_noches_y_bienvenidos"));
					buttons.push(new CSoundButton("Damas e cabaeros, Tim Ryans", "damas_e_cabaeros"));
					buttons.push(new CSoundButton("Es abasador...", "es_abasador"));
					buttons.push(new CSoundButton("Es un placer", "es_un_placer"));
					buttons.push(new CSoundButton("Estamos absolutamente encantadas", "estamos_absolutamente_encantadas"));
					buttons.push(new CSoundButton("Estamos muy orgullosos de la nevación", "estamos_muy_orgullosos"));
					buttons.push(new CSoundButton("Estar en Colonia", "estar_en_colonia"));
					buttons.push(new CSoundButton("Fabricantes de automotrices", "fabricantes_de_automotrices"));
					buttons.push(new CSoundButton("Gracias", "gracias"));
					buttons.push(new CSoundButton("Gran Turismo 6", "gran_turismo_6"));
					buttons.push(new CSoundButton("Interfaz de cuatro susarios", "interfaz_de_cuatro_susarios"));
					buttons.push(new CSoundButton("Les invito a mirarlos", "les_invito_a_mirarlos"));
					buttons.push(new CSoundButton("Los juegos pudorosos", "los_juegos_pudorosos"));
					buttons.push(new CSoundButton("Me gusta eso", "me_gusta_eso"));
					buttons.push(new CSoundButton("Me paso el tiempo jugar", "me_paso_el_tiempo_jugar"));
					buttons.push(new CSoundButton("Mercado alemán", "mercado_aleman"));
					buttons.push(new CSoundButton("¡Oh, sí!", "oh_si"));
					buttons.push(new CSoundButton("Fue Oscar Wilde quien dijo...", "oscar_wilde"));
					buttons.push(new CSoundButton("Pe-Pesetrés", "pe_pesetres"));
					buttons.push(new CSoundButton("Grand Station, Grand Theft", "play_station_grand_theft"));
					buttons.push(new CSoundButton("Por favor damas e cabaeros...", "por_favor_damas_y_caballeros"));
					buttons.push(new CSoundButton("Son software independiente", "son_software_independiente"));
					buttons.push(new CSoundButton("Vamos a anunzar una conjunta", "vamos_a_anunzar"));
					buttons.push(new CSoundButton("Vida en un mundo mágico", "vida_en_mundo_magico"));
					buttons.push(new CSoundButton("Y cuesh-suh... Yoshida", "y_cuesh_suh"));
					buttons.push(new CSoundButton("109-9 euros en Europa", "109-9_euros_en_europa"));
					buttons.push(new CSoundButton("Aspectos fundamentales", "aspectos_fundamentales"));
					buttons.push(new CSoundButton("Adictivo serie N", "adictivo_serie_n"));
					buttons.push(new CSoundButton("Aja-sou-devu", "aja_sou_devu"));
					buttons.push(new CSoundButton("Ahora quiero pasar la mano", "ahora_quiero_pasar_la_mano"));
					buttons.push(new CSoundButton("Ca-ca", "ca_ca"));
					buttons.push(new CSoundButton("Compartir y compantir", "compartir_y_compantir"));
					buttons.push(new CSoundButton("Completamente", "completamente"));
					buttons.push(new CSoundButton("Con una tangente de memoria", "con_una_tangente_memoria"));
					buttons.push(new CSoundButton("Crear nuevo contenido jego", "crear_nuevo_contenido_jego"));
					buttons.push(new CSoundButton("De-devu el año que viene", "de_devu_el_ano_que_viene"));
					buttons.push(new CSoundButton("De wanna brodels", "de_wanna_brodels"));
					buttons.push(new CSoundButton("Dedicar su tiempo y talento", "dedicar_su_tiempo_y_talento"));
					buttons.push(new CSoundButton("Mei-necrene-seinuquipo", "mei_necrene_seinuquipo"));
					buttons.push(new CSoundButton("Meterse en mundos torcidos", "meterse_en_mundos_torcidos"));
					buttons.push(new CSoundButton("Luftrausers", "luftrausers"));
					buttons.push(new CSoundButton("Luchanchut", "luchanchut"));
					buttons.push(new CSoundButton("Libertad para hacer lo que quieren", "libertad_para_hacer_lo_que_quieren_hacer"));
					buttons.push(new CSoundButton("Libertad para elegir aparato", "libertad_para_elegir_aparato"));
					buttons.push(new CSoundButton("Lego Marvel", "lego_marvel"));
					buttons.push(new CSoundButton("\"Indie\" significa libertad", "indie_significa_libertad"));
					buttons.push(new CSoundButton("If you die...", "if_you_die"));
					buttons.push(new CSoundButton("Hacer el ataturiata", "hacer_el_ataturiata"));
					buttons.push(new CSoundButton("Ha sido muy exitoso", "ha_sido_muy_exitoso"));
					buttons.push(new CSoundButton("Gracias, Mark", "gracias_mark"));
					buttons.push(new CSoundButton("Hacklefish: face de Phil Fish", "face_de_phil_fish"));
					buttons.push(new CSoundButton("En el norte de América", "en_el_norte_de_America"));
					buttons.push(new CSoundButton("Ellos siguen su visión", "ellos_siguen_su_vision"));
					buttons.push(new CSoundButton("El mundo de juegos", "el_mundo_de_juegos"));
					buttons.push(new CSoundButton("El lugar de los indies", "el_lugar_de_los_indies"));
					buttons.push(new CSoundButton("El año que viene", "el_ano_que_viene"));
					buttons.push(new CSoundButton("Ebrun-alamacmilan", "ebrun_alamacmilan"));
					buttons.push(new CSoundButton("Devu", "devu"));
					buttons.push(new CSoundButton("Desolladores", "desolladores"));
					buttons.push(new CSoundButton("Arasiu-devu", "arasiu_devu"));
					buttons.push(new CSoundButton("Va a hacer su devu", "va_a_hacer_su_devu"));
					buttons.push(new CSoundButton("Un rogue-like-like", "un_rogue_like_like"));
					buttons.push(new CSoundButton("Un revolución de desarrollo independiente", "un_revolucion_de_desarrollo_independiente"));
					buttons.push(new CSoundButton("Un desarrollador icono", "un_desarrollador_icono"));
					buttons.push(new CSoundButton("Un aparato amistoso", "un_aparato_amistoso"));
					buttons.push(new CSoundButton("Tenemos football manager", "tenemos_football_manager"));
					buttons.push(new CSoundButton("(Suspiro)", "suspiro"));
					buttons.push(new CSoundButton("Sus hijos no perfectos", "sus_hijos_no_perfectos"));
					buttons.push(new CSoundButton("Son ambishiosos", "son_ambishiosos"));
					buttons.push(new CSoundButton("Será gratuito", "sera_gratuito"));
					buttons.push(new CSoundButton("¡Queeee...!", "queee"));
					buttons.push(new CSoundButton("¿Qué es ser un indie?", "que_es_ser_un_indie"));
					buttons.push(new CSoundButton("Punto", "punto"));
					buttons.push(new CSoundButton("Próxima pene-prada", "proxima_pene_prada"));
					buttons.push(new CSoundButton("Piloto en zonas de guerras", "piloto_en_zonas_de_guerras"));
					buttons.push(new CSoundButton("Pese-bite", "pese_bite"));
					buttons.push(new CSoundButton("Pe-ese-cuattro", "pe_ese_cuato"));
					buttons.push(new CSoundButton("No sabemos dónde va Mike", "no_sabemos_donde_va_mike"));
					buttons.push(new CSoundButton("N++", "n_plus_plus"));
					buttons.push(new CSoundButton("14 estudios en 3 continentes", "14_estudios_en_3_continentes"));
					buttons.push(new CSoundButton("50 equipos y exipos", "50_equipos_y_exipos"));
					buttons.push(new CSoundButton("Alemanes wathever...", "alemanes_wathever"));
					buttons.push(new CSoundButton("Algunas de estas ideas...", "algunas_de_estas_ideas"));
					buttons.push(new CSoundButton("El juleshock, el juleshock", "el_juleshock_el_juleshock"));
					buttons.push(new CSoundButton("El servicio de Red Dead", "el_servicio_de_red_es"));
					buttons.push(new CSoundButton("Es la ancha", "es_la_ancha"));
					buttons.push(new CSoundButton("Estréngicos", "estrengicos"));
					buttons.push(new CSoundButton("Éxitos del mundo indie", "exitos_mas_grandes_del_mundo_indie"));
					buttons.push(new CSoundButton("Gracias, Shahirimarg", "gracias_shahirimac"));
					buttons.push(new CSoundButton("Juanca Uropa es un bar de vivir", "guancauropa_es_un_lugar_de_vivir"));
					buttons.push(new CSoundButton("Hotline Madammie", "hotline_madammie"));
					buttons.push(new CSoundButton("¡Max!", "max"));
					buttons.push(new CSoundButton("Ofrecer más detalles", "ofrecer_mas_detalles"));
					buttons.push(new CSoundButton("Paquete básico", "paquete_basico"));
					buttons.push(new CSoundButton("Reino Unido, Francia, Italia,...", "reino_unido_francia_italia"));
					buttons.push(new CSoundButton("Tenemos el premio de Gilda", "tenemos_el_premio_de_gilda"));
					buttons.push(new CSoundButton("¡Twitch!", "twitch"));
					buttons.push(new CSoundButton("Una disfunción amplia", "una_disfuncion_amplia"));
					buttons.push(new CSoundButton("Un interfaz nuevo", "una_interfaz_nuevo"));
					buttons.push(new CSoundButton("Parte esencial de nuestra estrategia", "una_parte_esencial_de_nuestra_estrategia"));
					
					// more_pirata
					buttons.push(new CSoundButton("7 días", "7_dias", false, "more_pirata"));
					buttons.push(new CSoundButton("14 días", "14_dias", false, "more_pirata"));
					buttons.push(new CSoundButton("¿Alguien quiere utilizar la televisión?", "alguien_quiere_utilizar_la_television", false, "more_pirata"));
					buttons.push(new CSoundButton("Assassin's Creed", "assassins_creed", false, "more_pirata"));
					buttons.push(new CSoundButton("Assassin's Creed es un franquicia", "assassins_creed_es_un_franquicia", false, "more_pirata"));
					buttons.push(new CSoundButton("Buenas tardes Su Excelencia", "buenas_tardes_su_excelencia", false, "more_pirata"));
					buttons.push(new CSoundButton("Comprendo", "comprendo", false, "more_pirata"));
					buttons.push(new CSoundButton("Con esta piel...", "con_esta_piel", false, "more_pirata"));
					buttons.push(new CSoundButton("Eché de menos esa reunión", "eche_de_menos_ese_reunion", false, "more_pirata"));
					buttons.push(new CSoundButton("El gayma...", "el_gayma", false, "more_pirata"));
					buttons.push(new CSoundButton("En wy-fy", "en_wifi", false, "more_pirata"));
					buttons.push(new CSoundButton("Epares-mas", "epares_mas", false, "more_pirata"));
					buttons.push(new CSoundButton("Es un placer estar aquí esta noche", "es_un_placer_estar_aqui", false, "more_pirata"));
					buttons.push(new CSoundButton("Esa es mi causa", "esa_es_mi_causa", false, "more_pirata"));
					buttons.push(new CSoundButton("Este país llamado Jack-dooo", "este_pais_llamado_jackdooo", false, "more_pirata"));
					buttons.push(new CSoundButton("Gracias, Jim", "gracias_jim", false, "more_pirata"));
					buttons.push(new CSoundButton("Gracias Pascal y a todos", "gracias_pascal_y_a_todos", false, "more_pirata"));
					buttons.push(new CSoundButton("Guano-focamos", "guanofocamos", false, "more_pirata"));
					buttons.push(new CSoundButton("Gui Guimó", "gui_guimo", false, "more_pirata"));
					buttons.push(new CSoundButton("Han sido 5 años...", "han_sido_5_anos", false, "more_pirata"));
					buttons.push(new CSoundButton("Hay que apuntar para la proa", "hay_que_apuntar_para_la_proa", false, "more_pirata"));
					buttons.push(new CSoundButton("Hola, If", "hola_if", false, "more_pirata"));
					buttons.push(new CSoundButton("Comprendo... vayámonos", "comprendo_vayamonos", false, "more_pirata"));
					buttons.push(new CSoundButton("Incremento para nuestros sevitones", "incremento_para_nuestros_sevitones", false, "more_pirata"));
					buttons.push(new CSoundButton("La fila de una hoja", "la_fila_de_una_hoja", false, "more_pirata"));
					buttons.push(new CSoundButton("La otra vez que hablamos", "la_otra_vez_que_hablamos", false, "more_pirata"));
					buttons.push(new CSoundButton("La verdad doloroso", "la_verdad_doloroso", false, "more_pirata"));
					buttons.push(new CSoundButton("Le vamos a dar demostración ahora", "le_vamos_a_dar_demostracion_ahora", false, "more_pirata"));
					buttons.push(new CSoundButton("Los jugadores valoran los consolas", "los_jugadores_valoran_los_consolas", false, "more_pirata"));
					buttons.push(new CSoundButton("Mantenemos el país", "mantenemos_el_pais", false, "more_pirata"));
					buttons.push(new CSoundButton("Me dijeron", "me_dijeron", false, "more_pirata"));
					buttons.push(new CSoundButton("Una metacrítica de 80", "metacritica_de_80", false, "more_pirata"));
					buttons.push(new CSoundButton("Mientras se escucha...", "mientras_se_escucha", false, "more_pirata"));
					buttons.push(new CSoundButton("No puedo regresar a África", "no_puedo_regresar_a_africa", false, "more_pirata"));
					buttons.push(new CSoundButton("No. Quizás... Bueno, no sé", "no_quizas_bueno_no_se", false, "more_pirata"));
					buttons.push(new CSoundButton("Nos emociona mucho", "nos_emociona_mucho", false, "more_pirata"));
					buttons.push(new CSoundButton("O Morteirõs están llegandow", "o_morteros_estan_llegandow", false, "more_pirata"));
					buttons.push(new CSoundButton("Parece fantástico", "parece_fantastico", false, "more_pirata"));
					buttons.push(new CSoundButton("Prefiero no cortar sus labios y comérselos...", "prefiero_no_cortar_sus_labios", true, "more_pirata"));
					buttons.push(new CSoundButton("Puede seguir hojatando en su aparato", "puede_seguir_hojatando", false, "more_pirata"));
					buttons.push(new CSoundButton("¿Qué hace un nuestro como usted...?", "que_hace_un_nuestro_como_usted", false, "more_pirata"));
					buttons.push(new CSoundButton("¿Qué vas a hacer con tu porción...?", "que_vas_a_hacer_con_tu_porcion", false, "more_pirata"));
					buttons.push(new CSoundButton("Reshugón", "reshugon", false, "more_pirata"));
					buttons.push(new CSoundButton("Sasins play steish creed three", "sasins_play_steish_creed_three", false, "more_pirata"));
					buttons.push(new CSoundButton("Se anidan los servicios adicionales", "se_anidan_los_servicios_adicionales", false, "more_pirata"));
					buttons.push(new CSoundButton("Todos ciudadanos", "se_todos_los_nombres", false, "more_pirata"));
					buttons.push(new CSoundButton("Seguir luchando", "seguir_luchando", false, "more_pirata"));
					buttons.push(new CSoundButton("Soy un esclavo", "soy_un_esclavo", false, "more_pirata"));
					buttons.push(new CSoundButton("Televisión de matrimonio", "television_de_matrimonio", true, "more_pirata"));
					buttons.push(new CSoundButton("Tenemos que destrocer esos cañones", "tenemos_que_destrocer_esos_canones", false, "more_pirata"));
					buttons.push(new CSoundButton("Tenemos que golpear el proa", "tenemos_que_golpear_el_proa", false, "more_pirata"));
					buttons.push(new CSoundButton("Tenemos una foto muy grande", "tenemos_una_foto_muy_grande", false, "more_pirata"));
					buttons.push(new CSoundButton("Tontería del televisión", "tonteria_del_television", false, "more_pirata"));
					buttons.push(new CSoundButton("Un PlayStation cuatro juego...", "un_playstation_cuatro_juego", false, "more_pirata"));
					buttons.push(new CSoundButton("Un poquito de tontería", "un_poquito_de_tonteria", false, "more_pirata"));
					buttons.push(new CSoundButton("Un valor extraordinario", "un_valor_extraordinario", false, "more_pirata"));
					buttons.push(new CSoundButton("¡Vamos a empezar con la acción!", "vamos_a_empezar_con_la_accion", false, "more_pirata"));
					buttons.push(new CSoundButton("¡VAS A REGRESAR A ÁFRICA!", "vas_a_regresar_a_africa", false, "more_pirata"));
					buttons.push(new CSoundButton("Velocidad lenta", "velocidad_lenta", false, "more_pirata"));
					buttons.push(new CSoundButton("Wachever y Maxdome", "wachever_y_maxdome", false, "more_pirata"));
					buttons.push(new CSoundButton("Yo conozcow... Le conozcow... Pirata", "yo_conozco_le_conozco", false, "more_pirata"));
					buttons.push(new CSoundButton("Yo nací en Trinidad", "yo_naci_en_trinidad", false, "more_pirata"));
					buttons.push(new CSoundButton("Yubicup, Yubisof", "yubicup_yubisof", false, "more_pirata"));
					
					// even_more_pirat
					buttons.push(new CSoundButton("60 manutos", "60_manutos", true, "even_more_pirata"));
					buttons.push(new CSoundButton("¡Alguien está dentro de la red!", "alguien_esta_dentro_de_la_red", true, "even_more_pirata"));
					buttons.push(new CSoundButton("Aún está emocionado", "aun_esta_emocionado", true, "even_more_pirata"));
					buttons.push(new CSoundButton("Blood bastack", "blood_bastard", true, "even_more_pirata"));
					buttons.push(new CSoundButton("Eh... carácter", "caracter", true, "even_more_pirata"));
					buttons.push(new CSoundButton("Eiden pies", "eiden_pies", true, "even_more_pirata"));
					buttons.push(new CSoundButton("[El guéimer]", "el_gueimer_tio", true, "even_more_pirata"));
					buttons.push(new CSoundButton("El mejor juego del show", "el_mejor_juego_del_show", true, "even_more_pirata"));
					buttons.push(new CSoundButton("Él no ha sido aceptado", "el_no_ha_sido_aceptao", true, "even_more_pirata"));
					buttons.push(new CSoundButton("El sistema no funciona", "el_sistema_no_funciona", true, "even_more_pirata"));
					buttons.push(new CSoundButton("Es alucinante", "es_alucinante", true, "even_more_pirata"));
					buttons.push(new CSoundButton("Es momento para despertarse", "es_momento_para_despertarse", true, "even_more_pirata"));
					buttons.push(new CSoundButton("Una tecnología sin precedentes", "es_tecnologia_sin_precedentes", true, "even_more_pirata"));
					buttons.push(new CSoundButton("¿Está en apuros?", "esta_en_apuros", true, "even_more_pirata"));
					buttons.push(new CSoundButton("Este va a ser el mejor año", "este_va_a_ser_el_mejor_ano", true, "even_more_pirata"));
					buttons.push(new CSoundButton("Hemos visto esa persona...", "hemos_visto_esa_persona", true, "even_more_pirata"));
					buttons.push(new CSoundButton("Los pirates están haciendo daño (de nuevo)", "los_pirates_estan_haciendo_dano", true, "even_more_pirata"));
					buttons.push(new CSoundButton("¡Me has metido un sistema!", "me_has_metido_un_sistema", true, "even_more_pirata"));
					buttons.push(new CSoundButton("Muchísimos gracias", "muchisimos_gracias", true, "even_more_pirata"));
					buttons.push(new CSoundButton("Muita gente aquí...", "muita_gente_aqui", true, "even_more_pirata"));
					buttons.push(new CSoundButton("NO", "no", true, "even_more_pirata"));
					buttons.push(new CSoundButton("Nosotros estamos en control", "nosotros_estamos_en_control", true, "even_more_pirata"));
					buttons.push(new CSoundButton("Nosotros ofrecemos seguridad", "nosotros_ofrecemos_seguridad", true, "even_more_pirata"));
					buttons.push(new CSoundButton("Noticias muy importantes", "noticias_muy_importantes", true, "even_more_pirata"));
					buttons.push(new CSoundButton("Para cambiar el mundo", "para_cambiar_el_mundo", true, "even_more_pirata"));
					buttons.push(new CSoundButton("Pidiendo durante tres años", "pidiendo_durante_tres_anos", true, "even_more_pirata"));
					buttons.push(new CSoundButton("¿Por qué?", "por_que", true, "even_more_pirata"));
					buttons.push(new CSoundButton("¡Quieren entrar al sistema!", "quieren_entrar_al_sistema", true, "even_more_pirata"));
					buttons.push(new CSoundButton("SÍ", "si", true, "even_more_pirata"));
					buttons.push(new CSoundButton("¡SÍ!", "sii", true, "even_more_pirata"));
					buttons.push(new CSoundButton("Es tiempo de escuchar la verdad", "tiempo_de_escuchar_la_verdad", true, "even_more_pirata"));
					buttons.push(new CSoundButton("Tiziados de shakago", "tiziados_de_shakago", true, "even_more_pirata"));
					buttons.push(new CSoundButton("Un solo hombre", "un_solo_hombre", true, "even_more_pirata"));
					buttons.push(new CSoundButton("Una gran aplauso para If", "una_gran_aplausa_para_if", true, "even_more_pirata"));
					buttons.push(new CSoundButton("Ustedes están unorables", "ustedes_estan_unorables", true, "even_more_pirata"));
					buttons.push(new CSoundButton("Ustedes han escuchado muchas mentiras", "ustedes_han_escuchado_muchas_mentiras", true, "even_more_pirata"));
					buttons.push(new CSoundButton("Va a ser muy importante", "va_a_ser_muy_importante", true, "even_more_pirata"));
					buttons.push(new CSoundButton("¿Vamos a hablar de esto otra vez...?", "vamos_a_hablar_de_esto_otra_vez", true, "even_more_pirata"));
					buttons.push(new CSoundButton("Yo puedo ver por qué", "yo_puedo_ver_por_que", true, "even_more_pirata"));
					buttons.push(new CSoundButton("Yubisó", "yubiso", true, "even_more_pirata"));
					break;
				
				case Main.ANTONIO_NAME:
					buttons.push(new CSoundButton("1 euro del café...", "1_euro_del_cafe", false, "antonio"));
					buttons.push(new CSoundButton("21 euros", "21_euros", false, "antonio"));
					buttons.push(new CSoundButton("¿21 euros por un café...?", "21_euros_por_un_cafe", false, "antonio"));
					buttons.push(new CSoundButton("22 de diciembre", "22_de_diciembre", false, "antonio"));
					buttons.push(new CSoundButton("A ver, Manuel...", "a_ver_manuel", false, "antonio"));
					buttons.push(new CSoundButton("¡Aaahí va ese cafelito!", "ahi_va_ese_cafelito", false, "antonio"));
					buttons.push(new CSoundButton("Antonio...", "antonio", false, "antonio"));
					buttons.push(new CSoundButton("¡Antoniooo...!", "antoniooo", false, "antonio"));
					buttons.push(new CSoundButton("Auuung...", "auuung", false, "antonio"));
					buttons.push(new CSoundButton("Bajas, le das un abrazo...", "bajas", false, "antonio"));
					buttons.push(new CSoundButton("Cobra tío, por favor", "cobra_tio_por_favor", false, "antonio"));
					buttons.push(new CSoundButton("Después de esta cerrarás", "despues_de_esta_cerraras", false, "antonio"));
					buttons.push(new CSoundButton("El mayor premio", "el_mayor_premio", false, "antonio"));
					buttons.push(new CSoundButton("Enhorabuena Antonio", "enhorabuena_antonio", false, "antonio"));
					buttons.push(new CSoundButton("Fin de la historia", "fin_de_la_historia", false, "antonio"));
					buttons.push(new CSoundButton("\"I no longer feel...\"", "i_no_longer_feel", false, "antonio"));
					buttons.push(new CSoundButton("\"I wanna go...\"", "i_wanna_go", false, "antonio"));
					buttons.push(new CSoundButton("¡Ja, ja, ji, ji!", "ja_ja_ji_ji", false, "antonio"));
					buttons.push(new CSoundButton("¡¡¡Manu!!!", "manu", false, "antonio"));
					buttons.push(new CSoundButton("¡Míralos, míralos!", "miralos_miralos", false, "antonio"));
					buttons.push(new CSoundButton("NO", "no", false, "antonio"));
					buttons.push(new CSoundButton("¡No voy a cerrar!", "no_voy_a_cerrar", false, "antonio"));
					buttons.push(new CSoundButton("¡Nos ha tocado el Gordo!", "nos_ha_tocado_el_gordo", false, "antonio"));
					buttons.push(new CSoundButton("Para una vez que no compro...", "para_una_vez", false, "antonio"));
					buttons.push(new CSoundButton("...pero tienes que bajar", "pero_tienes_que_bajar", false, "antonio"));
					buttons.push(new CSoundButton("(Piano)", "piano", false, "antonio"));
					buttons.push(new CSoundButton("Ponme un café", "ponme_un_cafe", false, "antonio"));
					buttons.push(new CSoundButton("Pues ya está", "pues_ya_esta", false, "antonio"));
					buttons.push(new CSoundButton("(Ruido de fondo)", "ruido_fondo", false, "antonio"));
					buttons.push(new CSoundButton("\"Someone here's a lie...\"", "someone_heres_a_lie", false, "antonio"));
					buttons.push(new CSoundButton("\"This we'll celebrate...\"", "this_well_celebrate", false, "antonio"));
					buttons.push(new CSoundButton("¿Un café?", "un_cafe", false, "antonio"));
					buttons.push(new CSoundButton("Un cafelito", "un_cafelito", false, "antonio"));
					buttons.push(new CSoundButton("¿Una copitah?", "una_copita", false, "antonio"));
					buttons.push(new CSoundButton("Y 20 euros de ésto...", "y_20_euros_de_esto", false, "antonio"));
					break;
					
				case Main.MIXED_NAME:
					buttons.push(new CSoundButton("(Chasquido)", "(chasquido)", false, "mixed_hits"));
					buttons.push(new CSoundButton("15 segundos, 1 minuto,...", "15_segundos_1_minuto", false, "mixed_hits"));
					buttons.push(new CSoundButton("3500 animaciones...", "3500_animaciones", false, "mixed_hits"));
					buttons.push(new CSoundButton("A nivel intuitivo", "a_nivel_intuitivo", false, "mixed_hits"));
					buttons.push(new CSoundButton("¡¡¡Aaay!!!", "aaaay", false, "mixed_hits"));
					buttons.push(new CSoundButton("¿...Ah sí?", "ah_si", false, "mixed_hits"));
					buttons.push(new CSoundButton("Amigo Félix", "amigo_felix", false, "mixed_hits"));
					buttons.push(new CSoundButton("¡Ay, campanera!", "ay_campanera", false, "mixed_hits"));
					buttons.push(new CSoundButton("¡Ba baaa...!", "ba_baaa", false, "mixed_hits"));
					buttons.push(new CSoundButton("Cómo te gusta", "como_te_gusta", false, "mixed_hits"));
					buttons.push(new CSoundButton("Con cuatro píxeles", "con_cuatro_pixeles", false, "mixed_hits"));
					buttons.push(new CSoundButton("Coño de la madre", "cono_de_la_madre", false, "mixed_hits"));
					buttons.push(new CSoundButton("Copiar mecánicas", "copiar_mecanicas", false, "mixed_hits"));
					buttons.push(new CSoundButton("Cuanta ternura y bondad", "cuanta_ternura_y_bondad", false, "mixed_hits"));
					buttons.push(new CSoundButton("Daba daba", "daba_daba", false, "mixed_hits"));
					buttons.push(new CSoundButton("Encandila a primera vista", "encandila_a_primera_vista", false, "mixed_hits"));
					buttons.push(new CSoundButton("Entretenerlo, divertirlo y sorprenderlo", "entretenerlo_divertirlo_sorprenderlo", false, "mixed_hits"));
					buttons.push(new CSoundButton("Entreteniendooo...", "entreteniendooo", false, "mixed_hits"));
					buttons.push(new CSoundButton("Es un reto", "es_un_reto", false, "mixed_hits"));
					buttons.push(new CSoundButton("¡Es una mierda!", "es_una_mierda", false, "mixed_hits"));
					buttons.push(new CSoundButton("Es vivaracha", "es_vivaracha", false, "mixed_hits"));
					buttons.push(new CSoundButton("Gigante de la industria", "gigante_de_la_industria", false, "mixed_hits"));
					buttons.push(new CSoundButton("¿La quieres escuchar?", "la_quieres_escuchar", false, "mixed_hits"));
					buttons.push(new CSoundButton("¡La Rata de Antequera!", "la_rata_de_antequera", false, "mixed_hits"));
					buttons.push(new CSoundButton("La sonda anal", "la_sonda_anal", false, "mixed_hits"));
					buttons.push(new CSoundButton("La tecla apropiada", "la_tecla_apropiada", false, "mixed_hits"));
					buttons.push(new CSoundButton("Po la verdá es que nunca...", "la_verdad_es_que", false, "mixed_hits"));
					buttons.push(new CSoundButton("Le van a venir al pelo", "le_van_a_venir_al_pelo", false, "mixed_hits"));
					buttons.push(new CSoundButton("Licor de herbas", "licor_de_herbas", false, "mixed_hits"));
					buttons.push(new CSoundButton("Lo mejor de lo mejor", "lo_mejor_de_lo_mejor", false, "mixed_hits"));
					buttons.push(new CSoundButton("Los nuevos superventas", "los_nuevos_superventas", false, "mixed_hits"));
					buttons.push(new CSoundButton("Mercadona", "mercadona", false, "mixed_hits"));
					buttons.push(new CSoundButton("Motorizada", "motorizada", false, "mixed_hits"));
					buttons.push(new CSoundButton("No es del todo malo", "no_es_del_todo_malo", false, "mixed_hits"));
					buttons.push(new CSoundButton("No han sido un éxito", "no_han_sido_un_exito", false, "mixed_hits"));
					buttons.push(new CSoundButton("No tanto en el análisis", "no_tanto_en_el_analisis", false, "mixed_hits"));
					buttons.push(new CSoundButton("No tien ni puta ideia", "no_tien_ni_puta_idea", false, "mixed_hits"));
					buttons.push(new CSoundButton("No tiene perdón", "no_tiene_perdon", false, "mixed_hits"));
					buttons.push(new CSoundButton("Obama & Self", "obama_and_self", false, "mixed_hits"));
					buttons.push(new CSoundButton("Oh, qué gusto...", "oh_que_gusto", false, "mixed_hits"));
					buttons.push(new CSoundButton("Otras veces pensando", "otras_veces_pensando", false, "mixed_hits"));
					buttons.push(new CSoundButton("Pájaros que vigilan de cerca", "pajaros_vigilan_de_cerca", false, "mixed_hits"));
					buttons.push(new CSoundButton("¡Paquete!", "paquete", false, "mixed_hits"));
					buttons.push(new CSoundButton("Paquete con paquete", "paquete_con_paquete", false, "mixed_hits"));
					buttons.push(new CSoundButton("¡Param, pam, pam!", "param_pam_pam", false, "mixed_hits"));
					buttons.push(new CSoundButton("¡Po mu bié!", "pos_mu_bie", false, "mixed_hits"));
					buttons.push(new CSoundButton("Un proyecto muy ambicioso", "proyecto_muy_ambicioso", false, "mixed_hits"));
					buttons.push(new CSoundButton("Pero qué coño netricista", "que_cono_netricista", false, "mixed_hits"));
					buttons.push(new CSoundButton("¿Qué dise usted?", "que_dice_usted", false, "mixed_hits"));
					buttons.push(new CSoundButton("¿Seremos capaces?", "seremos_capaces", false, "mixed_hits"));
					buttons.push(new CSoundButton("¿Sí...? Sí", "si_si", false, "mixed_hits"));
					buttons.push(new CSoundButton("Síí", "siii", false, "mixed_hits"));
					buttons.push(new CSoundButton("Sor Citroën es un gay", "sor_citroen_es_un_gay", false, "mixed_hits"));
					buttons.push(new CSoundButton("Un tipo de dos metros", "un_tipo_de_dos_metros", false, "mixed_hits"));
					buttons.push(new CSoundButton("Una versión libre con letra", "una_version_libre_con_letra", false, "mixed_hits"));
					buttons.push(new CSoundButton("Unas bellísimas personas", "unas_bellisimas_personas", false, "mixed_hits"));
					buttons.push(new CSoundButton("Viva una aventura", "viva_una_aventura", false, "mixed_hits"));
					
					// faltan porrero, RENFE, paloma, Mendoza, albert & luismi, valeriana, pollos hermanos, etc.
			} 
		}
		
		private function SetupButtonPositions(startingItemIndex:uint = 0):void
		{
			var current_col:uint = 0;
			var current_row:uint = 0;
			
			var button_width:Number = (new SoundButton()).width;
			var button_height:Number = (new SoundButton()).height;
			var num_cols:uint = 4;
			
			var button:CSoundButton;
			
			for (var i:uint = 0; i < startingItemIndex; i++) {
				button = buttons[i];
				if(button && contains(button)) removeChild(button);
			}
			
			for (i = startingItemIndex; i < buttons.length; i++) {
				button = buttons[i];
				button.x = HORIZONTAL_MARGINS + (HORIZONTAL_GAP * current_col) + (current_col * button.width);
				button.y = VERTICAL_MARGINS * 0.6 + (VERTICAL_GAP * current_row) + (current_row * button.height) + 75;
				addChild(button);
				current_col++;
				
				if (current_col > num_cols) {
					current_col = 0;
					current_row++;
				}
				
				if (current_row >= ((BUTTONS_PER_PAGE / num_cols) - 3)) {
					break;
				}
			}
			
			navigator.SetCurrentPage(currentPage + 1, Math.ceil(buttons.length / BUTTONS_PER_PAGE));
		}
		
		private function SetupNavigator():void
		{
			navigator.NextSignal.add(function():void {
				if (currentPage < Math.ceil(buttons.length / BUTTONS_PER_PAGE) - 1) {
					//navigator.NextEnabled = true;
					currentPage++;
				}
				
				SetupButtonPositions(BUTTONS_PER_PAGE * currentPage);
				
				//if (currentPage == Math.ceil(buttons.length / BUTTONS_PER_PAGE) - 1)
					//navigator.NextEnabled = false;
			});
			
			navigator.PreviousSignal.add(function():void {
				if (currentPage > 0) {
					//navigator.PreviousEnabled = true;
					currentPage--;
				}
				
				//if(currentPage == 0) navigator.PreviousEnabled = false;
				
				SetupButtonPositions(BUTTONS_PER_PAGE * currentPage);
			});
		}
		
		public function RefreshTitle():void
		{
			SetupTitleBar();
		}
		
	}

}