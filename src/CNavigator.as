package  
{
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	import flash.media.Sound;
	import org.osflash.signals.Signal;
	/**
	 * ...
	 * @author Albert Badosa Solé
	 */
	public class CNavigator extends MovieClip
	{
		public static const BUTTON_RELEASED:String = "released";
		public static const BUTTON_PRESSED:String = "pressed";
		public static const BUTTON_DISABLED:String = "disabled";
		
		private var asset:NavigatorButtons;
		
		protected var nextSignal:Signal;
		protected var prevSignal:Signal;
		
		public function CNavigator() 
		{
			asset = new NavigatorButtons();
			asset.next.gotoAndStop(BUTTON_RELEASED);
			asset.previous.gotoAndStop(BUTTON_RELEASED);
			
			addChild(asset);
			
			nextSignal = new Signal();
			prevSignal = new Signal();
			
			asset.next.addEventListener(MouseEvent.CLICK, OnClick);
			asset.next.addEventListener(MouseEvent.MOUSE_DOWN, OnMousePressed);
			asset.next.addEventListener(MouseEvent.MOUSE_UP, OnMouseReleased);
			asset.next.addEventListener(MouseEvent.MOUSE_OUT, OnMouseReleased);
			
			asset.previous.addEventListener(MouseEvent.CLICK, OnClick);
			asset.previous.addEventListener(MouseEvent.MOUSE_DOWN, OnMousePressed);
			asset.previous.addEventListener(MouseEvent.MOUSE_UP, OnMouseReleased);
			asset.previous.addEventListener(MouseEvent.MOUSE_OUT, OnMouseReleased);
		}
		
		private function OnClick(e:MouseEvent):void
		{
			if (e.currentTarget.name == "next") {
				nextSignal.dispatch();
			} else if (e.currentTarget.name == "previous") {
				prevSignal.dispatch();
			}
		}
		
		private function OnMousePressed(e:MouseEvent):void
		{
			(e.currentTarget as MovieClip).gotoAndStop(BUTTON_PRESSED);
		}
		
		private function OnMouseReleased(e:MouseEvent):void
		{
			(e.currentTarget as MovieClip).gotoAndStop(BUTTON_RELEASED);
		}
		
		public function get NextSignal():Signal
		{
			return nextSignal;
		}
		
		public function get PreviousSignal():Signal
		{
			return prevSignal;
		}
		
		public function SetCurrentPage(current:uint, total:uint):void
		{
			asset.page_indicator.text = asset.page_indicator_shadow.text = current + "/" + total;
		}
		
		public function set NextEnabled(value:Boolean):void
		{
			if (value) {
				asset.next.gotoAndStop(BUTTON_RELEASED);
				asset.next.mouseEnabled = true;
			}
			else {
				asset.next.gotoAndStop(BUTTON_DISABLED);
				asset.next.mouseEnabled = false;
			}
		}
		
		public function set PreviousEnabled(value:Boolean):void
		{
			if (value) {
				asset.previous.gotoAndStop(BUTTON_RELEASED);
				asset.previous.mouseEnabled = true;
			}
			else {
				asset.previous.gotoAndStop(BUTTON_DISABLED);
				asset.previous.mouseEnabled = false;
			}
		}
		
	}

}