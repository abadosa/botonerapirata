package 
{
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.external.ExternalInterface;
	
	/**
	 * ...
	 * @author Albert Badosa Solé
	 */
	public class Main extends Sprite 
	{
		public static const PIRATA_NAME:String = "PIRATA";
		public static const ANTONIO_NAME:String = "ANTONIO";
		public static const MIXED_NAME:String = "MIXED";
		
		private static var _customStage:Stage;
		
		protected var pirataBoard:CBoard;
		protected var antonioBoard:CBoard;
		protected var mixedBoard:CBoard;
		
		protected var pirataTab:BoardButton;
		protected var antonioTab:BoardButton;
		protected var mixedTab:BoardButton;
		
		protected var mixerButton:MixerButton;
		
		public function Main():void 
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			stage.scaleMode = StageScaleMode.NO_SCALE;
			
			_customStage = stage;
			
			pirataTab = new BoardButton();
			pirataTab.gotoAndStop("released");
			pirataTab.label_board_name.text = PIRATA_NAME;
			pirataTab.y = 10;
			pirataTab.x = 0;
			pirataTab.addEventListener(MouseEvent.CLICK, OnTabMouseEvent, false, 0, true);
			pirataTab.addEventListener(MouseEvent.MOUSE_DOWN, OnTabMouseEvent, false, 0, true);
			pirataTab.addEventListener(MouseEvent.MOUSE_UP, OnTabMouseEvent, false, 0, true);
			pirataTab.addEventListener(MouseEvent.MOUSE_OUT, OnTabMouseEvent, false, 0, true);
			
			antonioTab = new BoardButton();
			antonioTab.gotoAndStop("released");
			antonioTab.label_board_name.text = ANTONIO_NAME;
			antonioTab.y = 10;
			antonioTab.x = 270;
			antonioTab.addEventListener(MouseEvent.CLICK, OnTabMouseEvent, false, 0, true);
			antonioTab.addEventListener(MouseEvent.MOUSE_DOWN, OnTabMouseEvent, false, 0, true);
			antonioTab.addEventListener(MouseEvent.MOUSE_UP, OnTabMouseEvent, false, 0, true);
			antonioTab.addEventListener(MouseEvent.MOUSE_OUT, OnTabMouseEvent, false, 0, true);
			
			mixedTab = new BoardButton();
			mixedTab.gotoAndStop("released");
			mixedTab.label_board_name.text = MIXED_NAME;
			mixedTab.y = 10;
			mixedTab.x = 540;
			mixedTab.addEventListener(MouseEvent.CLICK, OnTabMouseEvent, false, 0, true);
			mixedTab.addEventListener(MouseEvent.MOUSE_DOWN, OnTabMouseEvent, false, 0, true);
			mixedTab.addEventListener(MouseEvent.MOUSE_UP, OnTabMouseEvent, false, 0, true);
			mixedTab.addEventListener(MouseEvent.MOUSE_OUT, OnTabMouseEvent, false, 0, true);
			
			mixerButton = new MixerButton();
			mixerButton.gotoAndStop("released");
			mixerButton.y = 80;
			mixerButton.x = 270;
			mixerButton.addEventListener(MouseEvent.CLICK, OnMixerMouseEvent, false, 0, true);
			mixerButton.addEventListener(MouseEvent.MOUSE_DOWN, OnMixerMouseEvent, false, 0, true);
			mixerButton.addEventListener(MouseEvent.MOUSE_UP, OnMixerMouseEvent, false, 0, true);
			mixerButton.addEventListener(MouseEvent.MOUSE_OUT, OnMixerMouseEvent, false, 0, true);
			
			pirataBoard = new CBoard(PIRATA_NAME);
			antonioBoard = new CBoard(ANTONIO_NAME);
			mixedBoard = new CBoard(MIXED_NAME);
			
			pirataBoard.init();
			antonioBoard.init();
			mixedBoard.init();
			
			addChild(pirataTab);
			addChild(antonioTab);
			addChild(mixedTab);
			addChild(mixerButton);
			
			addChild(pirataBoard);
			pirataBoard.y = 60;
		}
		
		public static function get customStage():Stage
		{
			return _customStage;
		}
		
		private function OnTabMouseEvent(e:MouseEvent):void
		{
			switch(e.type) {
				case MouseEvent.MOUSE_DOWN:
					(e.currentTarget as BoardButton).gotoAndStop("pressed");
					break;
				case MouseEvent.MOUSE_OUT:
				case MouseEvent.MOUSE_UP:
					(e.currentTarget as BoardButton).gotoAndStop("released");
					break;
				case MouseEvent.CLICK:
					if (e.currentTarget == pirataTab) {
						if (contains(antonioBoard)) removeChild(antonioBoard);
						else if (contains(mixedBoard)) removeChild(mixedBoard);
						
						if (!contains(pirataBoard)) {
							pirataBoard.RefreshTitle();
							addChild(pirataBoard);
							pirataBoard.y = 60;
						}
					} else if (e.currentTarget == antonioTab) {
						if (contains(pirataBoard)) removeChild(pirataBoard);
						else if (contains(mixedBoard)) removeChild(mixedBoard);
						
						if (!contains(antonioBoard)) {
							addChild(antonioBoard);
							antonioBoard.y = 60;
						}
					} else if (e.currentTarget == mixedTab) {
						if (contains(pirataBoard)) removeChild(pirataBoard);
						else if (contains(antonioBoard)) removeChild(antonioBoard);
						
						if (!contains(mixedBoard)) {
							addChild(mixedBoard);
							mixedBoard.y = 60;
						}
					}
					break;
			}
		}
		
		private function OnMixerMouseEvent(e:MouseEvent):void
		{
			switch(e.type) {
				case MouseEvent.CLICK:
					var urlRequest:URLRequest = new URLRequest("http://www.google.com");
					//http://www.albertbadosa.com/albert/mixer
					var jscommand:String = "window.open('file:///C:/Users/Abadosa/Desktop/dev/BotoneraMixer/bin/index.html','win','height=600,width=800,toolbar=no,scrollbars=no, maximizable=no');";
					var url:URLRequest = new URLRequest("javascript:" + jscommand + " void(0);");
					navigateToURL(url, "_self");

					break;
				case MouseEvent.MOUSE_DOWN:
					(e.currentTarget as MixerButton).gotoAndStop("pressed");
					break;
				case MouseEvent.MOUSE_OUT:
				case MouseEvent.MOUSE_UP:
					(e.currentTarget as MixerButton).gotoAndStop("released");
					break;
			}
		}
		
	}
	
}