package  
{
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	import flash.events.Event;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.net.URLRequest;
	import flash.utils.getDefinitionByName;
	/**
	 * ...
	 * @author Albert Badosa Solé
	 */
	public class CSoundButton extends MovieClip
	{
		public static const SOUNDS_FOLDER:String = "sounds";
		
		public static const BUTTON_RELEASED:String = "released";
		public static const BUTTON_PRESSED:String = "pressed";
		
		private var asset:SoundButton;
		
		protected var special:Boolean;
		
		protected var sound_name:String;
		protected var sound_data:String;
		protected var sound_bytes:Sound;
		protected var sound_loaded:Boolean = false;
		
		private var channel:SoundChannel;
		private var channelIsPlaying:Boolean = false;
		
		public function CSoundButton(label:String, sound:String, special:Boolean = false, folder:String = "") 
		{
			sound_name = label;
			sound_data = sound;
			this.special = special;
			
			asset = new SoundButton();
			asset.label.text = sound_name;
			asset.bg.gotoAndStop(BUTTON_RELEASED);
			
			asset.bg.highlight.visible = special;
			
			addChild(asset);
			
			addEventListener(MouseEvent.CLICK, OnClick);
			addEventListener(MouseEvent.MOUSE_DOWN, OnMousePressed);
			addEventListener(MouseEvent.MOUSE_UP, OnMouseReleased);
			addEventListener(MouseEvent.MOUSE_OUT, OnMouseReleased);
			
			if (folder != null && folder != "") folder += "/";
			
			var soundRequest:URLRequest = new URLRequest(SOUNDS_FOLDER + "/" + folder + sound_data + ".mp3");
			
			sound_bytes = new Sound(soundRequest);
			sound_bytes.addEventListener(Event.COMPLETE, OnSoundLoaded, false, 0, true);
			
			channel = new SoundChannel();
		}
		
		private function OnClick(e:MouseEvent):void
		{
			if (sound_loaded) {
				if (channelIsPlaying) {
					channel.stop();
					channelIsPlaying = false;
				}
				else {
					channel = sound_bytes.play();
					channel.addEventListener(Event.SOUND_COMPLETE, OnSoundCompleted, false, 0, true);
					channelIsPlaying = true;
				}
			}
		}
		
		private function OnMousePressed(e:MouseEvent):void
		{
			asset.bg.gotoAndStop(BUTTON_PRESSED);
		}
		
		private function OnMouseReleased(e:MouseEvent):void
		{
			asset.bg.gotoAndStop(BUTTON_RELEASED);
		}
		
		private function OnSoundLoaded(e:Event):void
		{
			sound_loaded = true;
		}
		
		private function OnSoundCompleted(e:Event):void
		{
			channelIsPlaying = false;
		}
		
	}

}